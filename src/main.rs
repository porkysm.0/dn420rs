mod keygen;

use std::sync::mpsc::{Sender, Receiver};
use std::sync::mpsc;
use std::{io, fs};
use std::borrow::BorrowMut;
use structopt::StructOpt;
use std::path::PathBuf;
use std::io::Write;


#[derive(Debug, StructOpt)]
struct Opts {
    #[structopt(short = "v")]
    version: i64,

    #[structopt(short = "n")]
    number: i64,

    #[structopt(short = "s")]
    spaces: bool,

    #[structopt(short = "t", default_value="1")]
    threads: i64,

    #[structopt(short = "o", parse(from_os_str))]
    output: Option<PathBuf>,
}

fn run_threads(args: Opts) -> Result<(), String> {
    if args.version > 2 || args.version < 1 {
        return Err(String::from(format!("invalid version: {}", args.version)))
    }

    let workload_size = args.number/args.threads;

    let (tx, rx): (Sender<Vec<u8>>, Receiver<Vec<u8>>) = mpsc::channel();

    let pool = rayon::ThreadPoolBuilder::new()
        .num_threads(args.threads as usize)
        .build()
        .unwrap();

    for _ in 0..args.threads {
        let t_tx = tx.clone();
        let version = args.version;

        pool.spawn(move || {
            let key_gen = match version {
                1 => keygen::KeyGenerator::new_v1(),
                2 => keygen::KeyGenerator::new_v2(),
                _ => keygen::KeyGenerator::new_v1(),
            };

            for _ in 0..workload_size {
                t_tx.send(key_gen.execute()).unwrap()
            }
        });
    }

    drop(tx);

    let mut key_buffer: Vec<u8> = vec![];

    for data in rx.into_iter().borrow_mut() {
        let key_fmt = keygen::format_key(data, !args.spaces);
        key_buffer.append(&mut key_fmt.as_bytes().to_vec());
    }

    if let Some(output_path) = args.output {
        let mut f = fs::File::create(output_path).unwrap();
        f.write(&*key_buffer).unwrap();
        return Ok(());
    }

    let stdout = io::stdout();
    let mut stdout = stdout.lock();

    let mut d: &[u8] = &key_buffer;
    io::copy(&mut d, &mut stdout).unwrap();

    Ok(())
}

fn main() {
    let args = Opts::from_args();

    run_threads(args).unwrap();
}
