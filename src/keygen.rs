use rand::Rng;
use string_builder::Builder;
use std::collections::HashMap;

pub fn format_key(key: Vec<u8>, new_line: bool) -> String {
    let mut b = Builder::default();

    for (i, stub) in key.iter().enumerate() {
        if i != 0 && i % 3 == 0 {
            b.append('-');
        }

        b.append(stub.to_string());
    }

    if new_line {
        b.append('\n');
    } else {
        b.append(' ');
    }

    b.string().unwrap()
}

fn attempt_keygen(worker_id: i64, key_len: i64, desired_sum: i64, exceptions: &HashMap<i64, i64>) -> Vec<u8> {
    let mut rng = rand::thread_rng();
    let mut result: Vec<u8> = vec![0; key_len as usize];

    let mut temp_sum = desired_sum;

    for i in 0..key_len {
        if exceptions.contains_key(&i) {
            result[i as usize] = *exceptions.get(&i).unwrap() as u8;
            continue;
        }

        if i == key_len - 1 {
            if temp_sum > 9 || temp_sum < 0 {
                //println!("retry {}", temp_sum);
                return attempt_keygen(worker_id, key_len, desired_sum, exceptions);
            }

            result[i as usize] = temp_sum as u8;
            continue
        }

        let mut key_stub = rng.gen_range(0..temp_sum.clamp(1, 10));

        while i < key_len && temp_sum - key_stub > 9 * (key_len - i - 1){
            let delta = temp_sum - 9 * (key_len - i - 1);
            if delta > 9 {
                //println!("extreme delta {}", delta);
                return attempt_keygen(worker_id, key_len, desired_sum, exceptions);
            }

            //println!("large delta i={}, temp_sum={}, key_stub={}, delta={}", i, temp_sum, key_stub, delta);
            key_stub = rng.gen_range(
                delta..temp_sum.clamp(1, 10));
        }

        result[i as usize] = key_stub as u8;
        temp_sum -= key_stub;
    }

    result
}

pub struct KeyGenerator {
    exc_table: HashMap<i64, i64>,
    key_len: i64,
    desired_sum: i64
}

impl KeyGenerator {
    pub fn new_v1() -> Self {
        let mut exception_table: HashMap<i64, i64> = HashMap::new();
        exception_table.insert(5, 7);

        Self {
            exc_table: exception_table,
            key_len: 9,
            desired_sum: 39,
        }
    }

    pub fn new_v2() -> Self {
        let mut exception_table: HashMap<i64, i64> = HashMap::new();
        exception_table.insert(8, 1);

        Self {
            exc_table: exception_table,
            key_len: 12,
            desired_sum: 60,
        }
    }

    pub fn execute(&self) -> Vec<u8> {
        attempt_keygen(0, self.key_len, self.desired_sum, &self.exc_table)
    }
}

#[cfg(test)]
mod keygen_tests {
    use crate::keygen::{KeyGenerator, format_key};

    #[test]
    fn test_format_key() {
        let key = vec![3, 7, 3, 9, 0, 7, 7, 3, 7];
        let key_fmt = format_key(key, true);

        assert_eq!(key_fmt, String::from("373-907-737\n"));
    }

    #[test]
    fn test_keygen_attempt_v1() {
        let k = KeyGenerator::new_v1();
        let key = k.execute();

        assert_eq!(key.len(), 9);

        let mut sum: i64 = 0;

        for (i, stub) in key.iter().enumerate() {
            if i == 5 {
                continue
            }

            sum += *stub as i64;
        }

        assert_eq!(sum, 39)
    }

    #[test]
    fn test_keygen_attempt_v2() {
        let k = KeyGenerator::new_v2();
        let key = k.execute();

        assert_eq!(key.len(), 12);

        let mut sum: i64 = 0;

        for (i, stub) in key.iter().enumerate() {
            if i == 8 {
                continue
            }

            sum += *stub as i64;
        }

        assert_eq!(sum, 60)
    }
}