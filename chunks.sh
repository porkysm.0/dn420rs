#!/bin/bash

NUM_THREADS=6
NUM_CHUNKS=5
NUM_KEYS=10
((PER_CHUNK=NUM_KEYS/NUM_CHUNKS))

for i in $(seq 1 $NUM_CHUNKS); do echo $i; ./dn420rs -n $PER_CHUNK -t $NUM_THREADS -v 1 >> output.txt; done