# dn420rs

```
USAGE:
    dn420rs [FLAGS] [OPTIONS] -n <number> -v <version>

FLAGS:
    -h, --help       Prints help information
    -s               
    -V, --version    Prints version information

OPTIONS:
    -n <number>         
    -o <output>         
    -t <threads>         [default: 1]
    -v <version>
```